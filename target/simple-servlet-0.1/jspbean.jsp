<!DOCTYPE html>

<html>
    <head>
        <title>JavaBeans in JSP</title>
    </head>

    <body>
    <jsp:useBean id="test" class="bean.CpuBean" />
    <!-- <jsp:setProperty
        name="test"
        property="message"
        value="WWW" /> -->
    <h1>CPU Description</h1>
    <p> CPU Name: <jsp:getProperty name="test" property="name" /> </p>
    <p> Clockspeed: <jsp:getProperty name="test" property="freq" /> </p>
    <p> Cores: <jsp:getProperty name="test" property="cores" /> </p>
    <p> Threads: <jsp:getProperty name="test" property="threads" /> </p>

    </body>
</html>

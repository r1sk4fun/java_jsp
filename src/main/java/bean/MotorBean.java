package bean;

public class MotorBean {
    private String name = "Rover V8";
    private String volume = "4552";
    private String horsepower = "224";
    private String torque = "2600";
    private String consumption = "15.9";

    public String getName() {
        return(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVolume() {
        return(volume);
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getHorsepower() {
        return(horsepower);
    }

    public void setHorsepower(String horsepower) {
        this.horsepower = horsepower;
    }

    public String getTorque() {
        return(torque);
    }

    public void setTorque(String torque) {
        this.torque = torque;
    }

    public String getConsumption() {
        return(consumption);
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>Пример JSP-страницы</title>
</head>
<body>
    <jsp:useBean id="motor" class="bean.MotorBean" scope="session" />
    
    <h1>Параметры двигателя</h1>
    
    <form action="" method="post">
        <label for="name">Название:</label>
        <input type="text" id="name" name="name" /><br/>
        
        <label for="freq">Объем двигателя, куб.см:</label>
        <input type="text" id="freq" name="freq" /><br/>

        <label for="cores">Максимальная мощность, л.с:</label>
        <input type="text" id="cores" name="cores" /><br/>

        <label for="threads">Максимальный крутящий момент, об./мин:</label>
        <input type="text" id="threads" name="threads" /><br/>

        <label for="threads">Расход топлива, л/100 км:</label>
        <input type="text" id="threads" name="threads" /><br/>
        
        <input type="submit" value="Submit" />
    </form>
    
    <jsp:setProperty name="motor" property="*" />
    
    <h2>Текущая модель двигателя:</h2>
    <p>Название: <jsp:getProperty name="motor" property="name" /></p>
    <p>Объем двигателя, куб.см: <jsp:getProperty name="motor" property="volume" /></p>
    <p>Максимальная мощность, л.с: <jsp:getProperty name="motor" property="horsepower" /></p>
    <p>Максимальный крутящий момент, об./мин: <jsp:getProperty name="motor" property="torque" /></p>
    <p>Расход топлива, л/100 км: <jsp:getProperty name="motor" property="consumption" /></p>
</body>
</html>
